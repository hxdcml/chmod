# Chmod #

To change unix permissions or 'change mode'


### What is this repository for? ###

Syntax
Options
Numeric mode
Symbolic mode
Examples
Related

### Chmood syntax ###

 chmod [Options]... Mode [,Mode]... file...

       chmod [Options]... Numeric_Mode file...

       chmod [Options]... --reference=RFile file...

### Chmod options ###

  -f, --silent, --quiet   suppress most error messages

  -v, --verbose           output a diagnostic for every file processed
  -c, --changes           like verbose but report only when a change is made

      --reference=RFile   use RFile's mode instead of MODE values

  -R, --recursive         change files and directories recursively

      --help              display help and exit

      --version           output version information and exit

### Chmod numeric mode ###

From one to four octal digits
Any omitted digits are assumed to be leading zeros. 

The first digit = selects attributes for the set user ID (4) and set group ID (2) and save text image (1)S
The second digit = permissions for the user who owns the file: read (4), write (2), and execute (1)
The third digit = permissions for other users in the file's group: read (4), write (2), and execute (1)
The fourth digit = permissions for other users NOT in the file's group: read (4), write (2), and execute (1)

The octal (0-7) value is calculated by adding up the values for each digit
User (rwx) = 4+2+1 = 7
Group(rx) = 4+1 = 5
World (rx) = 4+1 = 5
chmode mode = 0755

### Chmod symbolic mode ###
Read	r
Write	w
Execute (or access for directories)	x
Execute only if the file is a directory 
(or already has execute permission for some user)	X
Set user or group ID on execution	s
Save program text on swap device	t
The permissions that the User who owns the file currently has for it	u
The permissions that other users in the file's Group have for it	g
Permissions that Other users not in the file's group have for it	o

### Chmod examples ###
Deny execute permission to everyone: 
chmod a-x chmodExample.txt

Allow read permission to everyone:
chmod a+r chmodExample.txt

Make a file readable and writable by the group and others: 
chmod go+rw chmodExample.txt

Make a shell script executable by the user/owner 
$ chmod u+x chmodExample.sh

Allow everyone to read, write, and execute the file and turn on the set group-ID: 
chmod =rwx,g+s chmodExample.txt

### Related chmod links ###
- https://chmodcommand.com
- https://ss64.com/bash/chmod.html
- https://ss64.com/bash/ls.html
- https://help.ubuntu.com/community/FilePermissions
